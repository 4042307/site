import Layout from './default.layout.js'

export default () => kitten.html`
  <${Layout} pageId='what'>
    <markdown>
      [code nginx]
      # nginx configuration on 4042307.org
      location / {
        error_page 404 =307 https://2017.4042307.org$request_uri;
        try_files $uri $uri/ =404;
      }
      [code]

      __What if links never died?__ What if [we never broke the Web?](https://www.w3.org/Provider/Style/URI) What if it didn’t involve any extra work?

      It’s possible. And _easy_.

      Just make your 404s into 307s.

      ## Try it out:

        - A page from 2017: [/portfolio/better-blocker](/portfolio/better-blocker)
        - Another page from 2017: [/portfolio/accessibility-for-everyone](/portfolio/accessibility-for-everyone)
        - A page from 1997: [/me.htm](/me.htm)
        - A page that never existed: [/missing.html](/missing.html)

      ## Learn more:

      - [Why is this important?](/why/)
      - [How does it work?](/how/)
      - [Get started!](/get-started/)
    </markdown>
  </>
`
