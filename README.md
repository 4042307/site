# 404 → 307

__🌲 A simple gesture for an evergreen Web.__

**What if links never died?** What if we never broke the Web? What if it didn’t involve any extra work?

It’s possible. And _easy_.

_Just make your 404s into 307s._

## In general

1. Serve your current site from a subdomain (e.g., `2017.your.site`).

2. Add the 404 to 307 rule for your server (see below) to the latest version of your site to forward links that are not found on the latest version to the previous version.

3. If you change the site again in the future, rinse and repeat.

## Using Site.js

With [Site.js](https://sitejs.org) you have to do is to create a simple text file called `4042307` in the root directory of your site. In it, place the URL of the previous version of the site that you want to forward requests to that don’t exist on the current version.

e.g., in `/4042307`, put:

```
https://2017.4042307.org
```

## Using nginx

```nginx
# nginx
location / {
error_page 404 =307 https://2017.4042307.org$request_uri;
try_files $uri $uri/ =404;
}
```

## Learn more

  * [Visit the 4042307 web site.](https://4042307.org)
  * Read: [Why?](https://4042307.org/why)
  * Read: [How?](https://4042307.org/how)

[Get involved](https://4042307.org/how#contribute) and help out.

## Like this? Fund us!

[Small Technology Foundation](https://small-tech.org) is a tiny, independent not-for-profit.

We’re building the [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/).

We exist in part thanks to patronage by people like you. If you share [our vision](https://small-tech.org/about/#small-technology) and want to support our work, please [become a patron or donate to us](https://small-tech.org/fund-us) today and help us continue to exist.

---

**Canonical Git Remote:** https://codeberg.org/4042307/site
