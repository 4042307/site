import Layout from './default.layout.js'

export default () => kitten.html`
  <${Layout} pageId='getStarted'>
    <markdown>
      ## Get started!

      1. Serve your current site from a subdomain (e.g., [code]2017.your.site[code]).

      2. Add the 404 to 307 rule for your server (see below) to the latest version of your site to forward links that are not found on the latest version to the previous version.

      3. If you change the site again in the future, rinse and repeat.

      ## Kitten

      [Kitten](https://codeberg.org/kitten/app#kitten) natively understands 404 to 307. Just add the following to your _kitten.json_:

      ### kitten.json
      [code json]
      {
        "4042307": "https://2017.your.site"
      }
      [code]

      For more information, see the [404 → 307 section in the Kitten documentation](https://codeberg.org/kitten/app#404-307) and the [404 → 307 Kitten example](https://codeberg.org/kitten/app/src/branch/main/examples/4042307).

      ## nginx

      In your site configuration:

      [code nginx]
      server {
        # …
        location / {
          error_page 404 =307 https://2017.your.site$request_uri;
          try_files $uri $uri/ =404;
        }
      }
      [code]

      For example, if your site uses [Let’s Encrypt](https://letsencrypt.org) on Ubuntu, you should find your site configuration file in [code]/etc/nginx/sites-available/https.conf[code].

      ## WordPress
       [Fabrica Evergreen Web](https://github.com/aral/fabrica-evergreen-web/tree/307) is a tiny plugin which allows you to use 404 → 307 on WordPress sites. After installation and activation, you specify the fallback path on the settings page.

      ## Contribute

      [Edit this page on Codeberg](https://codeberg.org/4042307/site/src/branch/main/get-started.page.js) to contribute 404 → 307 configurations for your favourite servers and platforms and send a pull request.

        If you’re having trouble, please feel free to [open an issue](https://codeberg.org/4042307/site/issues).
    </markdown>
  </>
`
