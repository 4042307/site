export default ({ SLOT, pageId }) => {
  const pages = [
    { id: 'what', link: '/', name: 'What?' },
    { id: 'why', link: '/why/', name: 'Why?' },
    { id: 'how', link: '/how/', name: 'How?' },
    { id: 'getStarted', link: '/get-started/', name: 'Get started!' },
  ]
  pages.what = 0; pages.why = 1; pages.how = 2; pages.getStarted = 3

  return kitten.html`
    <page
      water
      highlightJS
      syntax-highlighting-theme='base16/ia-dark'
      title='404 → 307: ${pages[pages[pageId]].name}'
    >
    <header>
      <markdown>
        # [404 → 307](/)
        ## <span id='evergreenTree'>🌲</span> A simple gesture for an evergreen web.
      </markdown>
      <nav>
        <ul>
          ${pages.map(page => kitten.html`
            <if ${pageId === page.id}>
              <li class='currentPage'>${page.name}</li>
            <else>
              <li><a href='${page.link}'>${page.name}</li>
            </if>
          `)}
          <li><a href='https://codeberg.org/4042307/'><img class='icon' src='/icons/git.svg' alt='View source (git)'></a></li>
          <li><a href='https://mastodon.ar.al'><img class='icon' src='/icons/fediverse.png' alt='Fediverse logo (go to Aral’s Mastodon instance)'></a></li>
        </ul>
      </nav>
    </header>
    <main>
      ${SLOT}

      <div id='funding'>
        <markdown>
          ## 💕 Like this? Fund us!

          [Small Technology Foundation](https://small-tech.org) is a tiny, independent not-for-profit.

          We’re building the [Small Web](https://ar.al/2020/08/07/what-is-the-small-web/).

          We exist in part thanks to patronage by people like you. If you share [our vision](https://small-tech.org/about/#small-technology) and want to support our work, please [become a patron or donate to us](https://small-tech.org/fund-us) today and help us continue to exist.
        </markdown>
      </div>
    </main>
    <footer>
      <markdown>
        (c) 2018-${new Date().getUTCFullYear()} [Aral Balkan](https://ar.al), [Small Technology Foundation](https://small-tech.org). Licensed under [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1). 
      </markdown>
    </footer>

    <style>
      @media screen and (min-width: 839px) {
        nav, code, #funding { border-radius: 0.5em !important; }
      }

      html { font-size: max(18px, 2.25vmin); }

      body { padding: 1rem; margin: 0 auto; }

      header {
        margin-top: 0;

        & h1 {
          margin-top: 0;
          font-size: max(1.5em, 12vmin);
          font-weight: normal;
          text-align: center;
        }

        & h2 {
          font-size: max(1em, 3vmin);
          font-weight: normal;
          text-align: center;
          margin-top: -1em;
          color: #757575 !important;
        }

        & a {
          color: inherit;

          &:hover {
            text-decoration: none;
          }
        }

        & nav {
          display: flex;
          justify-content: center;
          background-color: var(--background-alt);

          margin: 1em -1rem 1em -1rem;
          padding: 0 1rem;
          line-height: 2;

          font-size: max(1.25rem, 3vmin);
          text-align: center;

          & ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
          }

          & li {
            display: inline-block;
            margin-right: 1em;
          }

          & li:last-of-type { margin-right: 0; }
        }
      }

      pre {
        white-space: pre-wrap;
        /* Offset padding to make text in page align flush left. */
        margin: 1em -1rem;
      }

      code {
        border-radius: 0;
        padding-bottom: 0;
      }

      footer {
        font-size: smaller;
        text-align: center;
      }

      .icon {
        height: 1.5rem;
        padding-bottom: 0.25rem;
        vertical-align: middle;
      }

      .currentPage {
        border-bottom: 3px solid var(--selection);
      }

      #evergreenTree {
        font-size: 1.5em;
        /* Optical adjustment to centre line visually. */
        margin-left: -0.5em;
      }

      #funding {
        background-color: var(--background-alt);
        padding: 1rem;
        margin-left: -1rem;
        margin-right: -1rem;

        & h2 { margin-top: -0.25em; /* optical adjustment */ }
        & p:last-of-type { margin-bottom: 0; }
      }
    </style>
  `
}
